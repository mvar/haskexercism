module CustomSet
  ( delete
  , difference
  , empty
  , fromList
  , insert
  , intersection
  , isDisjointFrom
  , isSubsetOf
  , member
  , null
  , size
  , toList
  , union
  , rotateLeft
  , rotateRight
  , getValue
  , getNode
  , CustomSet(..)
  , Direction(..)
  ) where

import Prelude hiding (null)
import Data.Maybe (fromJust)
import Data.List (foldl')

-- pretty print
-- import Text.Show.Pretty (ppShow)
-- import Language.Haskell.HsColour
-- import Language.Haskell.HsColour.Colourise
-- import System.IO
-- let colorPrint = putStrLn . hscolour TTY defaultColourPrefs False False "" False . ppShow
-- :set -interactive-print colorPrint

data Direction = L | R
data CustomSet a = Empty | Node a
                           (CustomSet a)
                           (CustomSet a) deriving (Eq, Show, Ord)

getNode :: CustomSet a -> Direction -> CustomSet a
getNode (Node _ l _) L = l
getNode (Node _ _ r) R = r
getNode Empty _ = Empty

getValue :: CustomSet a -> Maybe a
getValue Empty = Nothing
getValue (Node v _ _) = Just v

delete :: (Eq a, Ord a) => a -> CustomSet a -> CustomSet a
delete _ Empty = Empty
delete _ (Node _ Empty Empty) = Empty
delete x (Node v l r)
  | x < v = Node v (delete x l) r
  | x > v = Node v l (delete x r)
  | x == v = if r == Empty
             then l
             else Node inorderSuccessorValue l newTree
  | otherwise = Node v l r
  where
    inorderSuccessorValue = fromJust . getValue $ inorderSuccessor
    inorderSuccessor = getInorderSuccessor r
    newTree = removeInorderSuccessor r

getInorderSuccessor :: CustomSet a -> CustomSet a
getInorderSuccessor Empty = Empty
getInorderSuccessor (Node v Empty r) = Node v Empty r
getInorderSuccessor (Node _ l _) = getInorderSuccessor l

removeInorderSuccessor :: CustomSet a -> CustomSet a
removeInorderSuccessor Empty = Empty
removeInorderSuccessor (Node _ Empty r) = r
removeInorderSuccessor (Node v l r) = Node v (removeInorderSuccessor l) r

empty :: CustomSet a
empty = Empty

fromList :: Ord a => [a] -> CustomSet a
fromList [] = Empty
fromList xs = foldl (\x y -> insert y x) Empty xs

insert :: (Eq a, Ord a) => a -> CustomSet a -> CustomSet a
insert x Empty = Node x Empty Empty
insert x (Node v l r)
  | x == v = Node v l r
  | x < v = Node v (insert x l) r
  | otherwise = Node v l (insert x r)

member :: (Eq a, Ord a) => a -> CustomSet a -> Bool
member _ Empty = False
member x (Node v l r)
  | x == v = True
  | x < v = member x l
  | x > v = member x r
  | otherwise = False

null :: Eq a => CustomSet a -> Bool
null set = set == empty

size :: CustomSet a -> Int
size Empty = 0
size (Node _ l r) = 1 + size l + size r

toList :: CustomSet a -> [a]
toList Empty = []
toList (Node v l r) = v : ((toList l) ++ (toList r))

union :: (Ord a) => CustomSet a -> CustomSet a -> CustomSet a
union setA setB = foldl' (flip insert) setA (toList setB)

difference :: (Ord a) => CustomSet a -> CustomSet a -> CustomSet a
difference Empty _ = Empty
difference setA Empty = setA
difference setA setB = foldl' (flip delete) setA (toList setB)

intersection :: Ord a => CustomSet a -> CustomSet a -> CustomSet a
intersection setA setB = fromList $ [fromJust x | x <- intsect, x /= Nothing]
  where
    intsect = map (\x -> if (member x setB) then Just x else Nothing) (toList setA)

isDisjointFrom :: Ord a => CustomSet a -> CustomSet a -> Bool
isDisjointFrom Empty _ = True
isDisjointFrom setA setB = intersection setA setB == Empty

isSubsetOf :: Ord a => CustomSet a -> CustomSet a -> Bool
isSubsetOf Empty _ = True
isSubsetOf setA setB
  | False `elem` truth = False
  | otherwise = True
  where
    truth = map (flip member setB) (toList setA)

rotateLeft :: CustomSet a -> CustomSet a
rotateLeft Empty = Empty
rotateLeft (Node v l Empty) = Node v l Empty
rotateLeft (Node v l r) = Node (fromJust . getValue $ r) (Node v l rleft) rright
  where
    rleft = getNode r L
    rright = getNode r R

rotateRight :: CustomSet a -> CustomSet a
rotateRight Empty = Empty
rotateRight (Node v Empty r) = Node v Empty r
rotateRight (Node v l r) = Node (fromJust . getValue $ l) lleft (Node v lright r)
  where
    lleft = getNode l L
    lright = getNode l R
