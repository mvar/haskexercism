module PigLatin (translate) where

import Data.Maybe
import Data.List (intercalate)

vowels :: String
vowels = "aeiou"

consonant :: String
consonant = "bcdfghjklmnpqrstvxyz"

translate :: String -> String
translate xs = intercalate " " (map ruler (words xs))

ruler :: String -> String
ruler xs = if firstRule xs == Nothing
           then if thirdRule xs == Nothing
                then if secondRule xs == Nothing
                     then if fourthRule xs == Nothing
                          then xs
                          else fromJust (fourthRule xs)
                     else fromJust (secondRule xs)
                else fromJust (thirdRule xs)
           else fromJust (firstRule xs)

firstRule :: String -> Maybe String
firstRule xs
  | head xs `elem` vowels || firstTwo == "xr" || firstTwo == "yt" = Just (xs ++ "ay")
  | otherwise = Nothing
  where
    firstTwo = take 2 xs

secondRule :: String -> Maybe String
secondRule xs
  | head xs `elem` consonant && (head (tail xs)) `notElem` consonant =
    Just (tail xs ++ (head xs:"ay"))
  | otherwise = Nothing

thirdRule :: String -> Maybe String
thirdRule xs
  | take 2 xs == "qu" = Just (drop 2 xs ++ take 2 xs ++ "ay")
  | head xs `elem` consonant && take 2 (tail xs) == "qu" =
    Just (drop 3 xs ++ take 3 xs ++ "ay")
  | otherwise = Nothing

fourthRule :: String -> Maybe String
fourthRule xs
  | fourthRuler xs == [] = Nothing
  | otherwise = Just (fourthRuler xs)

fourthRuler :: String -> String
fourthRuler [] = []
fourthRuler (x:xs)
  | length (x:xs) == 2 && tail (x:xs) == "y" = "y" ++ x:"ay"
  | x `elem` consonant =
      if length cluster > 1
      then noncluster ++ cluster ++ "ay"
      else fourthRuler noncluster
  | otherwise = fourthRuler xs
  where
    noncluster = drop (length cluster) (x:xs)
    cluster = takeWhile (\y -> y `elem` consonant && y /= 'y') (x:xs)
