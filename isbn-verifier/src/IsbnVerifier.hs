module IsbnVerifier (isbn) where
import Data.Char

decodeIsbn :: String -> Int -> Int
decodeIsbn [] mul = 0
decodeIsbn (x:xs) mul
  | mul == 1 =
    if x == 'X'
    then 10 * mul
    else digitToInt x
  | mul > 1 =
    if x == 'X'
    then 0
    else ((digitToInt x) * mul) + decodeIsbn xs (mul - 1)
  | otherwise = 0

isbnValidLength :: String -> Bool
isbnValidLength str = length (filterIsbn str) == 10

filterIsbn :: String -> String
filterIsbn str = filter (\x -> x == 'X' || (isDigit x)) str

isbnValid10th :: String -> Bool
isbnValid10th [] = False
isbnValid10th str = not ((isAlpha x) && (x /= 'X'))
  where
    x = last str

isbnModValid :: String -> Bool
isbnModValid str = mod (decodeIsbn (filterIsbn str) 10) 11 == 0

isbnLegalCharacters :: String -> Bool
isbnLegalCharacters str = length (filter (\x -> x /= 'X' && (isAlpha x)) str) == 0

isbn :: String -> Bool
isbn str = isbnModValid str && isbnValid10th str && isbnValidLength str && isbnLegalCharacters str
