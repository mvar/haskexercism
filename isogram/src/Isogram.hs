module Isogram (isIsogram) where
import Data.Char
import Data.List

duplicateLettersList :: String -> String
duplicateLettersList [] = []
duplicateLettersList (x:[]) = []
duplicateLettersList (x:y:rest)
  | x == y = x : duplicateLettersList rest
  | otherwise = duplicateLettersList (y:rest)

isIsogram :: String -> Bool
isIsogram str = duplicateLettersList (sort (map toLower (filter isLetter str))) == []
