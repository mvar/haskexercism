module BST
    ( BST
    , bstLeft
    , bstRight
    , bstValue
    , empty
    , fromList
    , insert
    , singleton
    , toList
    ) where

data BST a = Empty | Node a (BST a) (BST a) deriving (Eq, Show)

bstLeft :: Ord a => BST a -> Maybe (BST a)
bstLeft (Node a Empty right) = Just (Node a Empty right)
bstLeft (Node a left right)  = Just left

bstRight :: Ord a => BST a -> Maybe (BST a)
bstRight (Node a left Empty) = Just (Node a left empty)
bstRight (Node a left right) = Just right

bstValue :: BST a -> Maybe a
bstValue Empty = Nothing
bstValue (Node a left right) = Just a

empty :: BST a
empty = Empty

fromList :: Ord a => [a] -> BST a
fromList xs = foldr insert Empty $ reverse xs

insert :: Ord a => a -> BST a -> BST a
insert x Empty = Node x Empty Empty
insert x (Node y left right)
  | x < y = Node y (insert x left) right
  | x > y = Node y left (insert x right)
  | otherwise = Node x left right

singleton :: a -> BST a
singleton x = Node x Empty Empty

toList :: BST a -> [a]
toList Empty = []
toList (Node a left right) = toList left ++ [a] ++ toList right
