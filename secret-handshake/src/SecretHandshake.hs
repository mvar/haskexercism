module SecretHandshake (handshake) where

import Data.Bits

reverseList :: [String] -> [String]
reverseList [] = []
reverseList (str:rest) = reverseList rest ++ [str]

normalizeList :: [String] -> [String]
normalizeList [] = []
normalizeList (str:rest)
  | str == "reverse" = normalizeList rest
  | otherwise = str : normalizeList rest

decodeNumber :: Int -> [String]
decodeNumber num 
  | num .&. 1 == 1 = "wink" : decodeNumber (num - 1)
  | num .&. 2 == 2 = "double blink" : decodeNumber (num - 2)
  | num .&. 4 == 4 = "close your eyes" : decodeNumber (num - 4)
  | num .&. 8 == 8 = "jump" : decodeNumber (num - 8)
  | num .&. 16 == 16 = "reverse" : decodeNumber (num - 16)
  | otherwise = []

handshake :: Int -> [String]
handshake n =
  if elem "reverse" x
  then reverseList (normalizeList x)
  else normalizeList x
    where
      x = decodeNumber n
