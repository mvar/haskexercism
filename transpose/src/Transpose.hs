module Transpose (transpose) where

import qualified Data.List as DL

transpose :: [String] -> [String]
transpose str = DL.transpose newstr
  where
    newstr = scanr (\x y ->
                   if length y > length x
                   then x ++ (take (length y - length x) $ repeat ' ')
                   else x) [] str
