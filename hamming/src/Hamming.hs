module Hamming (distance) where

compareStrings :: String -> String -> Int
compareStrings (x:xs) (y:ys)
  | x /= y = 1 + compareStrings xs ys
  | otherwise = 0 + compareStrings xs ys
compareStrings _ _ = 0

distance :: String -> String -> Maybe Int
distance xs ys
  | xs == ys = Just 0
  | length xs > length ys = Nothing
  | length ys > length xs = Nothing
  | otherwise = Just (compareStrings xs ys)
