module Series (Error(..), largestProduct) where
import Data.List as L
import Data.Vector as V
import Data.Char

data Error = InvalidSpan | InvalidDigit Char deriving (Show, Eq)

largestProduct :: Int -> String -> Either Error Integer
largestProduct size digits = prod
  where
    vec = V.mapM digitizer (V.fromList digits)
    prod = productizer size vec

productizer :: Int -> Either Char (Vector Int) -> Either Error Integer
productizer _ (Left x) = Left (InvalidDigit x)
productizer size (Right v)
  | size > V.length v || size < 0 = Left InvalidSpan
  | otherwise = Right (fromIntegral prod)
  where
    prod = L.maximum products
    products = L.map V.product (slicer 0 size v)

slicer :: Num a => Int -> Int -> Vector a -> [Vector a]
slicer index size vec =
  if index + size > (V.length vec)
  then []
  else V.slice index size vec : slicer (index+1) size vec

digitizer :: Char -> Either Char Int
digitizer x
  | isDigit x = Right (digitToInt x)
  | otherwise = Left x
