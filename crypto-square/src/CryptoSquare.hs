module CryptoSquare (encode) where
import Data.Char
import Data.List
import Data.List.Split

normalizeString :: String -> String
normalizeString xs = filter isAlphaNum xs

lowerCaseString :: String -> String
lowerCaseString [] = []
lowerCaseString (x:xs) = (toLower x) : lowerCaseString xs

encode :: String -> String
encode xs = unwords . transpose $ chunksOf chunkSize normalizedString
  where
    normalizedString = normalizeString . lowerCaseString $ xs
    chunkSize = ceiling . sqrt . fromIntegral . length
                . normalizeString . lowerCaseString $ xs
