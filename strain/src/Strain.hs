module Strain (keep, discard) where

discard :: (a -> Bool) -> [a] -> [a]
discard p [] = []
discard p (x:rest)
  | p x == False = x : discard p rest
  | otherwise = discard p rest

keep :: (a -> Bool) -> [a] -> [a]
keep p [] = []
keep p (x:rest)
  | p x == True = x : keep p rest
  | otherwise = keep p rest

