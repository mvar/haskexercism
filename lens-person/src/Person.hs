{-# LANGUAGE TemplateHaskell #-}
module Person
  ( Address (..)
  , Born    (..)
  , Name    (..)
  , Person  (..)
  , bornStreet
  , renameStreets
  , setBirthMonth
  , setCurrentStreet
  ) where

import Data.Time.Calendar (Day, toGregorian, fromGregorian)
import Control.Lens hiding (element)

data Person = Person { _name    :: Name
                     , _born    :: Born
                     , _address :: Address
                     }

data Name = Name { _foreNames :: String
                 , _surName   :: String
                 }

data Born = Born { _bornAt :: Address
                 , _bornOn :: Day
                 }

data Address = Address { _street      :: String
                       , _houseNumber :: Int
                       , _place       :: String
                       , _country     :: String
                       }

makeLenses 'Person
makeLenses 'Name
makeLenses 'Born
makeLenses 'Address

bornStreet :: Born -> String
bornStreet born = view (bornAt . street) born
--bornStreet born = born ^. bornAt . street

setCurrentStreet :: String -> Person -> Person
setCurrentStreet newStreet person = person & address . street .~ newStreet

setBirthMonth :: Int -> Person -> Person
setBirthMonth newMonth person = person & born . bornOn .~ setNewDate
  where
    setNewDate = fromGregorian year newMonth day
    (year, month, day) = toGregorian (person ^. born . bornOn)

renameStreets :: (String -> String) -> Person -> Person
renameStreets f person = person
                         & born . bornAt . street %~ f
                         & address . street %~ f
