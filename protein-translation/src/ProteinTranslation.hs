module ProteinTranslation(proteins) where
import Data.List

codonToProtein :: String -> String
codonToProtein str
  | str == "AUG" = "Methionine"
  | str == "UGG" = "Tryptophan"
  | str == "UUU" || str == "UUC" = "Phenylalanine"
  | str == "UUA" || str == "UUG" = "Leucine"
  | str == "UAU" || str == "UAC" = "Tyrosine"
  | str == "UGU" || str == "UGC" = "Cysteine"
  | str == "UCU" || str == "UCC" ||
    str == "UCA" || str == "UCG" = "Serine"
  | otherwise = "STOP"

analyzeRna :: String -> [String]
analyzeRna [] = []
analyzeRna str =
  if codonToProtein cod == "STOP"
  then []
  else (codonToProtein cod) : (analyzeRna rest)
  where
    (cod, rest) = splitAt 3 str
  
proteins :: String -> Maybe [String]
proteins str = Just (analyzeRna str)
