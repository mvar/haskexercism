module CollatzConjecture (collatz, collatzNonRec) where

reduceNum :: Integer -> Integer
reduceNum 1 = 0
reduceNum x = 1 + reduceNum (if x `rem` 2 == 0
                             then x `div` 2
                             else (3 * x) + 1)

collatz :: Integer -> Maybe Integer
collatz x
  | x <= 0 = Nothing
  | otherwise = Just (reduceNum x)

collatzNonRec :: Integer -> Maybe Integer
collatzNonRec x
  | x <= 0 = Nothing
  | otherwise = Just (fromIntegral (length $ (takeWhile (>1) (iterate f x))))
  where f = (\y -> if y `rem` 2 == 0
                   then y `div` 2
                   else (3*y) + 1)
