module Main where

import CollatzConjecture
import Criterion
import Criterion.Main

main :: IO ()
main = defaultMain
  [ bgroup "cc" [
      bench "collatz 100000000"         $ nf collatz       100000000
      , bench "collatzNonRec 100000000" $ nf collatzNonRec 100000000
      , bench "collatz 100000000"       $ nf collatz       100000000 
      , bench "collatzNonRec 100000000" $ nf collatzNonRec 100000000 ]
  ]
