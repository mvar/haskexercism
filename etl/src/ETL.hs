module ETL (transform) where

import Control.Category ((>>>))
-- (>>>) composes two arrows. The output of the first one is fed to the second.

import Data.Map (Map, toList, fromList)
import Data.Char (toLower)

transform :: Map a String -> Map Char a
-- without control.category:
-- transform strMap = fromList . concatMap transformPair $ (toList strMap)
transform = toList >>> concatMap transformPair >>> fromList
 where
  transformPair (score, letters) = map (tuple score) letters
  tuple score letter = (toLower letter, score)
