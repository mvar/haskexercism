module Allergies (Allergen(..), allergies, isAllergicTo) where
import Data.Maybe

data Allergen = Eggs
              | Peanuts
              | Shellfish
              | Strawberries
              | Tomatoes
              | Chocolate
              | Pollen
              | Cats
              deriving (Eq, Show)

allergenPoints :: Int -> Maybe Allergen
allergenPoints 1   = Just Eggs
allergenPoints 2   = Just Peanuts
allergenPoints 4   = Just Shellfish
allergenPoints 8   = Just Strawberries
allergenPoints 16  = Just Tomatoes
allergenPoints 32  = Just Chocolate
allergenPoints 64  = Just Pollen
allergenPoints 128 = Just Cats
allergenPoints  _  = Nothing

powersOfTwo :: [Int]
powersOfTwo = iterate (*2) 1

toBin :: Int -> [Int]
toBin 0 = [0]
toBin x = [x `rem` 2] ++ toBin (x `quot` 2)

zipper :: [Int] -> [Int] -> [Int]
zipper [] _ = []
zipper _ [] = []
zipper (x:xs) (y:ys) =
  if y == 1
  then x : zipper xs ys
  else zipper xs ys

isAllergicTo :: Allergen -> Int -> Bool
isAllergicTo allergen score = allergen `elem` allergens
  where
    allergens = map fromJust $ fmap allergenPoints scoreValues
    scoreValues = zipper powersOfTwo binValues
    binValues = take 8 (toBin score)

allergies :: Int -> [Allergen]
allergies score = map fromJust $ fmap allergenPoints scoreValues
  where
    scoreValues = zipper powersOfTwo binValues
    binValues = take 8 (toBin score) -- take up to 8 bits
