module Matrix
    ( Matrix
    , cols
    , column
    , flatten
    , fromList
    , fromString
    , reshape
    , row
    , rows
    , shape
    , transpose
    ) where

import qualified Data.Vector as V (Vector, fromList, (!), concat)
import Data.Vector.Split as VS (chunksOf)
import Data.List.Split (splitOn)

type Matrix a = [V.Vector a]

cols :: Matrix Int -> Int
cols [] = 0
cols (m:_) = length m

column :: Int -> Matrix Int -> V.Vector Int
column x matrix = trmatrix !! x
  where
    trmatrix = transpose matrix

flatten :: Matrix Int -> V.Vector Int
flatten matrix = V.concat matrix

fromList :: [[Int]] -> Matrix Int
fromList xss = map V.fromList xss

fromString :: String -> Matrix Int
fromString "" = []
fromString xs = fromList $ map (map (\x -> read x :: Int) . words) splt
  where
    splt = splitOn "\n" xs

reshape :: (Int, Int) -> Matrix Int -> Matrix Int
reshape (_,c) matrix = VS.chunksOf c $ V.concat matrix

row :: Int -> Matrix Int -> V.Vector Int
row x matrix = matrix !! x

rows :: Matrix Int -> Int
rows [] = 0
rows matrix = length matrix

shape :: Matrix Int -> (Int, Int)
shape matrix = (a,b)
  where
    a = rows matrix
    b = cols matrix

transpose :: Matrix Int -> Matrix Int
transpose matrix = transposer 0 len matrix
  where
    len = length $ matrix !! 0

transposer :: Int -> Int -> Matrix Int -> Matrix Int
transposer _ _ [] = []
transposer index len matrix =
  if index >= len
  then []
  else (V.fromList $ map (V.! index) matrix) : (transposer (index+1) len matrix)

