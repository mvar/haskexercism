module TwelveDays (recite) where

gifts :: Int -> String
gifts x
  | x == 1    = "a Partridge in a Pear Tree."
  | x == 2    = "two Turtle Doves"
  | x == 3    = "three French Hens"
  | x == 4    = "four Calling Birds"
  | x == 5    = "five Gold Rings"
  | x == 6    = "six Geese-a-Laying"
  | x == 7    = "seven Swans-a-Swimming"
  | x == 8    = "eight Maids-a-Milking"
  | x == 9    = "nine Ladies Dancing"
  | x == 10   = "ten Lords-a-Leaping"
  | x == 11   = "eleven Pipers Piping"
  | x == 12   = "twelve Drummers Drumming"
  | otherwise = "Ooops"

xmasDay :: Int -> String
xmasDay x
  | x == 1    = "first"
  | x == 2    = "second"
  | x == 3    = "third"
  | x == 4    = "fourth"
  | x == 5    = "fifth"
  | x == 6    = "sixth"
  | x == 7    = "seventh"
  | x == 8    = "eighth"
  | x == 9    = "ninth"
  | x == 10   = "tenth"
  | x == 11   = "eleventh"
  | x == 12   = "twelfth"
  | otherwise = "Ooops"

returnGifts :: Int -> Int -> String
returnGifts start stop
  | stop == 1 && start == 1 = gifts stop
  | stop == 1 && start > 1  = "and "
                              ++ gifts stop
  | otherwise = (gifts stop)
                ++ ", "
                ++ returnGifts start (stop - 1)

recite :: Int -> Int -> [String]
recite start stop
  | start > stop = []
  | otherwise = ("On the "
                 ++ xmasDay start
                 ++ " day of Christmas my true love gave to me, "
                 ++ (returnGifts start start))
                : (recite (start+1) stop)
