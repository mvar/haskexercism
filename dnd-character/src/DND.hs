module DND ( Character(..)
           , ability
           , modifier
           , character
           ) where

import Test.QuickCheck

data Character = Character
  { name         :: String
  , strength     :: Int
  , dexterity    :: Int
  , constitution :: Int
  , intelligence :: Int
  , wisdom       :: Int
  , charisma     :: Int
  , hitpoints    :: Int
  }
  deriving (Show, Eq)

modifier :: Int -> Int
modifier x = div (x-10) 2

ability :: Gen Int
ability = do
  a <- choose (3,18)
  return a

character :: Gen Character
character = do
  nam <- arbitrary
  str <- ability
  dex <- ability
  con <- ability
  int <- ability
  wis <- ability
  cha <- ability
  return (Character nam str dex con int wis cha ((modifier con) + 10))

