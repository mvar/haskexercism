module BinarySearch (find) where

import Data.Array

find :: (Ord a, Eq a, Num a) => Array Int a -> a -> Maybe Int
find ar x = find' (mn, mx) ar x
  where
    (mn, mx) = bounds ar
    find' (start, end) arr i
      | end - start == 1 && arr ! start == i = Just start
      | end - start == 1 && arr ! end == i = Just end
      | start > end || end - start == 1 = Nothing
      | otherwise = if arr ! middle == i
                    then Just middle
                    else if i < arr ! middle
                         then find' (start, middle) arr i
                         else find' (middle, end) arr i
      where
        middle = (start + end) `div` 2
