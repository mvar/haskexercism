module Change (findFewestCoins) where

import Data.Set (empty, toList, fromList, filter)
import Data.List (tails)
import Data.Maybe (listToMaybe)

draw :: [a] -> Int -> [[a]]
draw _ 0 = [[]]
draw xs n = [t0 : rest | t@(t0:_) <- tails xs, rest <- draw t (n-1)]

findFewestCoins :: Integer -> [Integer] -> Maybe [Integer]
findFewestCoins target coins
  | target == 0 = Just []
  | target < 0 || target < head coins = Nothing
  | otherwise = listToMaybe result
  where
    result = go 1 (fromList $ draw coins 1)
    go n xs
      | n > (fromIntegral target) = []
      | at /= empty = toList at
      | otherwise = go (n+1) (fromList $ draw coins (n+1))
      where
        at = Data.Set.filter (\cs -> sum cs == target) xs
