module Luhn (isValid) where

import Data.Char
import Data.List.Split (chunksOf)

reverse' :: [Char] -> [Char]
reverse' = rev []
  where
    rev y [] = y
    rev y (x:xs) =
      if isSpace x
      then rev y xs
      else rev (x:y) xs

isValid :: String -> Bool
isValid "0" = False
isValid n = res `mod` 10 == 0
  where
    res = sum (zipWith (+) a prod)
    prod = map digitDouble b
    (a, b) = takeEverySecond . reverse' $ n

digitDouble :: Int -> Int
digitDouble x =
  if prod > 9
  then prod - 9
  else prod
  where prod = 2 * x

takeEverySecond :: [Char] -> ([Int], [Int])
takeEverySecond xs = (a, b)
  where
    a = map (digitToInt . head) chunks
    b = map (digitToInt . last) chunks
    chunks = chunksOf 2 xs
