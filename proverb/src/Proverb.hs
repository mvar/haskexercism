module Proverb(recite) where

loopList :: String -> [String] -> String
loopList [] (initial:y:rest) = "For want of a " ++ initial ++
                            " the " ++ y ++ " was lost.\n" ++ loopList initial (y:rest)
loopList  initial (x:y:rest) = "For want of a " ++ x ++ " the " ++ y ++
                            " was lost.\n" ++ loopList initial (y:rest)
loopList  initial (z:rest)
  | rest == [] = "And all for the want of a " ++ initial ++ "."
  | otherwise = "For want of a " ++ z ++ " the " ++ (show rest) ++
    " was lost\n" ++ loopList initial []

recite :: [String] -> String
recite [] = ""
recite (s:[]) = "And all for the want of a " ++ s ++ "."
recite str = loopList "" str
