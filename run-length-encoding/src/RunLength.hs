module RunLength (decode, encode) where
import Data.Char
import Data.List

countWords :: String -> Int -> String
countWords [] c = []
countWords (y:[]) c =
  if c > 1
  then show c ++ [y]
  else [y]
countWords (x:y:rest) c
  | x == y = countWords (y:rest) (c+1)
  | otherwise =
    if c > 1
    then show c ++ [x] ++ countWords (y:rest) 1
    else [x] ++ countWords (y:rest) 1

findDigits :: String -> (String, String)
findDigits [] = ([],[])
findDigits (x:str) =
  if isDigit x
  then span isDigit (x:str)
  else (['0'],x:str)

decode :: String -> String
decode [] = []
decode str = (replicate times current) ++ (decode rest)
  where
    (num, current:rest) = findDigits str
    times =
      if num == ['0']
      then 1
      else read num :: Int

encode :: String -> String
encode text = countWords text 1
