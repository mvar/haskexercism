module Base (Error(..), rebase) where
import Data.List

data Error a = InvalidInputBase | InvalidOutputBase | InvalidDigit a deriving (Show, Eq)

rebase :: Integral a => a -> a -> [a] -> Either (Error a) [a]
rebase inputBase outputBase inputDigits
  | inputBase  < 2 = Left InvalidInputBase
  | outputBase < 2 = Left InvalidOutputBase
  | otherwise = invalidDigits inputBase inputDigits >>= toDecimal >>= toBase
  where
    toBase = decToAny outputBase
    toDecimal = anyToDec inputBase

anyToDec :: Integral a => a -> [a] -> Either (Error a) a
anyToDec base digits = Right (sum $ zipWith (basePower base) pwr digits)
  where
    pwr = reverse [0..len]
    len = (length digits) - 1
    basePower b p x = x * b^p

decToAny :: Integral a => a -> a -> Either (Error a) [a]
decToAny base quotient = Right (reverse $ unfoldr remquot quotient)
  where
    remquot b = if b == 0
                then Nothing
                else Just (b `rem` base,b `quot` base)

-- invalidDigits :: Integral a => a -> [a] -> [a] -> Either (Error a) [a]
-- invalidDigits _ initial [] = Right initial
-- invalidDigits base initial (x:xs)
--   | x < 0 || x >= base = Left (InvalidDigit x)
--   | otherwise = invalidDigits base initial xs


invalidDigits :: Integral a => a -> [a] -> Either (Error a) [a]
invalidDigits base = sequence . fmap invalidDigit -- or just "traverse invalidDigit"
 where
   invalidDigit x
     | x < 0 || x >= base = Left (InvalidDigit x)
     | otherwise = Right x

-- invalidDigits :: Integral a => a -> [a] -> Either (Error a) [a]
-- invalidDigits base initial = go initial
--   where
--     go [] = Right initial
--     go (x:xs)
--       | x < 0 || x >= base = Left (InvalidDigit x)
--       | otherwise = go xs
