module Triangle (rows) where
import Data.Char

-- void printPascal(int n) 
-- { 
-- for (line = 1; line <= n; line++) 
-- { 
--     int C = 1; // used to represent C(line, i) 
--     for (i = 1; i <= line; i++)  
--     { 
--         printf("%d ", C); // The first value in a line is always 1 
--         C = C * (line - i) / i;  
--     } 
--     printf("\n"); 
-- } 
-- } 

outerLoop :: Integer -> [[Integer]]
outerLoop x
  | x > 0 = (innerLoop 1 x 1) : (outerLoop (x-1))
  | otherwise = []

innerLoop :: Integer -> Integer -> Integer -> [Integer]
innerLoop i line c
  | i > line = []
  | otherwise = c : innerLoop (i+1) line (quot (c * (line - i)) i)

rows :: Integer -> [[Integer]]
rows x = reverse (outerLoop x)
