module Prime (nth) where
import Data.List.Ordered

primes :: [Int]
primes = 2 : 3 : minus [5,7..] (unionAll [ [p*p, p*p+2*p..] | p <- tail primes])

nth :: Int -> Maybe Int
nth 0 = Nothing
nth n = Just (last . take n $ primes)
