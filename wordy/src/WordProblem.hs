module WordProblem (answer) where

import Text.Parsec (oneOf, many, chainl1, char, string, parse, try, (<|>))
import Text.Parsec.Token (integer)
import Text.Parsec.Language (haskell)
import Text.Parsec.String (Parser)

opers :: Parser (Integer -> Integer -> Integer)
opers =  try (matchPlus >> return (+))
         <|> try (matchMinus >> return (-))
         <|> try (matchMul >> return (*))
         <|> try (matchDiv >> return div)

matchWhitespace :: Parser String
matchWhitespace = many (oneOf " \t\n")

matchWhat :: Parser String
matchWhat = string "What is"

matchQmark :: Parser Char
matchQmark = char '?'

matchPlus :: Parser String
matchPlus = string "plus"

matchDiv :: Parser String
matchDiv = string "divided by"

matchMul :: Parser String
matchMul = string "multiplied by"

matchMinus :: Parser String
matchMinus = string "minus"

answer :: String -> Maybe Integer
answer question = case parse wordy "" question of
  Left _ -> Nothing
  Right i -> Just i
  where
    wordy = matchWhat *> chainl1 (matchWhitespace *> integer haskell) opers <* matchQmark

