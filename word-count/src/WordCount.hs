module WordCount (wordCount) where

import Data.MultiSet (MultiSet)
import Data.Char
import qualified Data.Text as T
import qualified Data.MultiSet as MS

removeCommas :: T.Text -> T.Text
removeCommas str = T.map (\x -> if x == ',' then ' ' else x) str

chompString :: T.Text -> T.Text
chompString str = T.dropAround (not . isAlphaNum) str

wordCount :: String -> [(T.Text, MS.Occur)]
wordCount xs = MS.toOccurList . MS.deleteAll T.empty $ ms
  where
    ms = MS.fromList $ fmap chompString wrds
    wrds = T.words . removeCommas . T.pack $ str
    str = map toLower xs
