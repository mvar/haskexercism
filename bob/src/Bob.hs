module Bob (responseFor) where
import Data.Char
import Safe (lastMay)

isQuestion :: String -> Bool
isQuestion str = lastMay str == Just '?'

isYell :: String -> Bool
isYell str =
  if any isUpper str
  then all isUpper (filter isLetter str)
  else False

isEmpty :: String -> Bool
isEmpty = null

responseFor :: String -> String
responseFor xs
  | isQuestion str && not (isYell str) = "Sure."
  | isQuestion str = "Calm down, I know what I'm doing!"
  | isYell str = "Whoa, chill out!"
  | isEmpty str = "Fine. Be that way!"
  | otherwise = "Whatever."
  where
    str = filter (not . isSpace) xs
