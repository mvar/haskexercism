module Main where

import DNA
import Criterion
import Criterion.Main

dnastr = "GATGCCCCTAAGAACCTCTCGGTCGACGCAAGCGATTACACTCCTGTCACATCATAATCGTTTGCTATTCAGGGCTTGACCAACACTGGATTGCTTTTCACTTAAAGTATTATGCACGACAGGGTGCGTGTACCATGTAAACCTGTTATAACTTACCTCAGACTAGTTGGAAGTGTGGCTAGATCTTAGCTTACGTCACTAGAGGGTCCACGTTTAGTTTTTAAGATCCATTGATCTCCTAAACGCTGCAAGATTCGCAACCTGGTATACTTAGCGCTAGGTCCTAGTGCAGCGGGACTTTTTTTCTAAAGTCGTTGAGAGGAGGAGTCGTCAGACCAGATAGCTTTGATGTCCTGATCGGAAGGATCGTTGGCCCCCGACCCTTAGACTCTGTACTCAGTTCTATAAACGAGCCATTGGATACGAGATCCGTAGATTGATAAGGGACACGGAATATCCCCGGACGCAATAGACGGACAGCTTGGTATCCTGAGCACAGTCGCGCGTCCGAATCTAGCTCTACTTTAGAGGCCCCGGATTCTGATGGTCGTAGACCGCAGAACCGATTGGGGGGATGTACAACAATATCTGTTAGTCACCTTTGGGTCACGGTCTGCTACCTTACTGGAATTTAGTCCGTCCTATAATTTCCCTTGCATATAAGTTGCGTTACTTCGGCCTCCTAACCGCACCCTTAGCACGAAGACAGATTCGTTCTTACCCATACTCCACCGTTGGCAGGGGGATCGCATGTCCCACGTGAAACATTGCTAAACCCTCAGGTCTCTGAGCGACAAAAGCTTTAAAGGGAAATTCGCGCCCATAACTTGGTCCGAATACGGGTTCTTGCATCGTTCGACTGAGTTTGTTTTATATAAAACGGGCGCAATGTCTGCTTTGATCAACCTCCAATACCTCGTATCATTGTGCACCTGCCGGTGACCACTCAACGATGTGGGGACGCCGTTGCAACTTCGAGGACCTAATGTGACCGACCTAG"

main :: IO ()
main = defaultMain
  [ bgroup "cc" [
      bench "toRNA mapM"       $ nf toRNA  dnastr
      , bench "toRNA non-mapM" $ nf toRNA2 dnastr ]
  ]
