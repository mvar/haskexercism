module DNA (toRNA,toRNA2) where

-- non-mapM solution

dnaToRna2 :: Char -> Char
dnaToRna2 'A' = 'U'
dnaToRna2 'C' = 'G'
dnaToRna2 'G' = 'C'
dnaToRna2 'T' = 'A'
dnaToRna2  s  =  s

isValid :: Char -> Bool
isValid s = s `elem` "ACGT"

toRNA2 :: String -> Either Char String
toRNA2 xs =
  if null invalidStrand
  then Right rnaSequence
  else Left invalidSequenceLead
  where
    (validStrand, invalidStrand) = span isValid xs
    invalidSequenceLead = head invalidStrand
    rnaSequence = map dnaToRna2 validStrand

-- proper with mapM

dnaToRna :: Char -> Either Char Char
dnaToRna 'A' = Right 'U'
dnaToRna 'C' = Right 'G'
dnaToRna 'G' = Right 'C'
dnaToRna 'T' = Right 'A'
dnaToRna  s  = Left   s

toRNA :: String -> Either Char String
toRNA xs = mapM dnaToRna xs
