module ArmstrongNumbers (armstrong) where
import Data.Char

armstrong :: Integral a => a -> Bool
armstrong x = foldl (+) 0 digitList == number
  where
    digitList = map (\y -> (digitToInt y)^len) numStr
    len = length numStr
    numStr = show number
    number = fromIntegral x
