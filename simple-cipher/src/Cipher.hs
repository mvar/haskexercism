module Cipher (caesarDecode, caesarEncode, caesarEncodeRandom) where

import Data.List ((++))
import Data.Vector (Vector, fromList, head, last, elemIndex, (!), elemIndices)
import Data.Maybe (fromJust)
import Prelude hiding (head, last)
import Data.Char (toLower)
import System.Random

mkName :: IO String
mkName = do
  g <- newStdGen
  return (take 100 $ randomRs ('a','z') g)

alphabet :: Vector Char
alphabet = fromList $ ['a'..'z'] ++ ['a'..'z']

caesarDecode :: String -> String -> String
caesarDecode key text
  | key == [] || text == [] = ""
  | otherwise = if ey == []
                then decodedChar : caesarDecode (k:[]) ext
                else decodedChar : caesarDecode ey ext
  where
    decodedChar = alphabet ! jumpIndex
    jumpIndex = abs $ (-) tIndex kIndex
    tIndex = last $ elemIndices t alphabet
    kIndex = head $ elemIndices k alphabet
    (k:ey) = key
    (t:ext) = text

caesarEncode :: String -> String -> String
caesarEncode key text
  | key == [] || text == [] = ""
  | otherwise = if tIndex /= Nothing && kIndex /= Nothing
                then
                  if ey == []
                  then decodedChar : caesarEncode (k:[]) ext
                  else decodedChar : caesarEncode ey ext
                else ""
  where
    decodedChar = alphabet ! jumpIndex
    jumpIndex = fromJust $ (+) <$> tIndex <*> kIndex
    tIndex = elemIndex t alphabet
    kIndex = elemIndex k alphabet
    (k:ey) = key
    (t:ext) = text

caesarEncodeRandom :: String -> IO (String, String)
caesarEncodeRandom text = do
  key <- mkName
  pure (key, caesarEncode key str)
  where
    str = map toLower text
