module Brackets (arePaired) where

import Data.Map.Strict

-- https://rafal.io/posts/haskell-queues.html
-- plus some other ideas from existing solutions

data Queue a = Queue [a] deriving (Show, Eq)

emptyQueue :: Queue a
emptyQueue = Queue []

push :: a -> Queue a -> Queue a
push e (Queue es) = Queue ([e] ++ es)

pop :: Queue a -> (Maybe a, Queue a)
pop q =
  case top of
    Nothing -> (top, emptyQueue)
    Just e  -> (Just e, rest)
  where
    (top, rest) = peek q

peek :: Queue a -> (Maybe a, Queue a)
peek (Queue []) = (Nothing, emptyQueue)
peek (Queue q)  = (Just $ head q, Queue $ tail q)

bracketsMap :: Map Char Char
bracketsMap = fromList [(']','['), ('}','{'), (')', '(')]

analyzeQueue :: Queue Char -> String -> Bool
analyzeQueue q [] = q == emptyQueue
analyzeQueue q (x:xs)
  | x `elem` "[{(" = analyzeQueue (push x q) xs
  | x `elem` "]})" = case (pop q) of
                       (Nothing,_) -> False
                       (Just e, newQueue) ->
                         if bracketsMap ! x == e
                         then analyzeQueue newQueue xs
                         else False
  | otherwise = analyzeQueue q xs

arePaired :: String -> Bool
arePaired str = analyzeQueue emptyQueue str
