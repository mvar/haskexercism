module Beer (song) where

singBeerSong :: Integer -> String
singBeerSong 0 = "No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, 99 bottles of beer on the wall.\n"
singBeerSong 1 = "1 bottle of beer on the wall, 1 bottle of beer.\nTake it down and pass it around, no more bottles of beer on the wall.\n\n" ++ singBeerSong 0
singBeerSong 2 = "2 bottles of beer on the wall, 2 bottles of beer.\nTake one down and pass it around, 1 bottle of beer on the wall.\n\n" ++ singBeerSong 1
singBeerSong x = bottleNum ++ " bottles of beer on the wall, " ++ bottleNum ++
  " bottles of beer.\nTake one down and pass it around, " ++ newBottleNum ++ " bottles of beer on the wall.\n\n" ++ singBeerSong (x-1)
  where
    bottleNum = show x
    newBottleNum = show (x-1)

song :: String
song = singBeerSong 99

