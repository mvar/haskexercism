module Garden
  ( Plant (..)
  , garden
  , lookupPlants
  ) where

import Data.Map (Map)
import qualified Data.Map.Strict as Map
import Data.List.Split (chunksOf, splitOn)
import Data.List (transpose, sort)
import Data.Maybe (fromJust)

data Plant = Clover
           | Grass
           | Radishes
           | Violets
           | Invalid
           deriving (Eq, Show)

type Garden = Map String [Plant]

plant :: Char -> Plant
plant 'R' = Radishes
plant 'C' = Clover
plant 'G' = Grass
plant 'V' = Violets
plant  _  = Invalid

garden :: [String] -> String -> Garden
garden students plants = Map.fromList $ zip (sort students) mappedPlants
  where
    mappedPlants = map (map plant) $ normalizedPlants
    normalizedPlants = map concat $ transpose $ map (chunksOf 2) $ splitOn "\n" plants
-- ["VVVV","CGRC"] <- [["VV","VV"],["CG","RC"]] <- [["VV","CG"],["VV","RC"]] <- ["VVCG","VVRC"]

-- Map.lookup returns Maybe
lookupPlants :: String -> Garden -> [Plant]
lookupPlants student grdn = fromJust (Map.lookup student grdn)
