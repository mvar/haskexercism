module Palindromes (largestPalindrome, smallestPalindrome) where
import Data.List (maximumBy, minimumBy)
import Data.Function (on)
import Data.Map (fromListWith,toList)

largestPalindrome :: Integer -> Integer -> Maybe (Integer, [(Integer, Integer)])
largestPalindrome minFactor maxFactor =
  if palindromes == []
  then Nothing
  else Just ((maximumBy (compare `on` fst) palindromes))
  where
    lst = [minFactor..maxFactor]
    palindromes = toList $ fromListWith (++) [(k, [v]) | (k, v) <- pairs lst]

smallestPalindrome :: Integer -> Integer -> Maybe (Integer, [(Integer, Integer)])
smallestPalindrome minFactor maxFactor =
  if palindromes == []
  then Nothing
  else Just ((minimumBy (compare `on` fst) palindromes))
  where
    lst = [minFactor..maxFactor]
    palindromes = toList $ fromListWith (++) [(k, [v]) | (k, v) <- pairs lst]

isPalindrome :: Integer -> Bool
isPalindrome x = xstr == reverse xstr
  where
    xstr = show x

pairs :: [Integer] -> [(Integer,(Integer, Integer))]
pairs list = [(x*y,(x,y)) | x <- list, y <- list, isPalindrome (x*y), x <= y]
