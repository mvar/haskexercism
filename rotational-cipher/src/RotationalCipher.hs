module RotationalCipher (rotate) where

import Data.Vector (Vector, fromList, elemIndex, (!))
import Data.Char (isAlpha, isUpper, toLower, toUpper)
import Data.Maybe

alphabet :: Vector Char
alphabet = fromList $ ['a'..'z'] ++ ['a'..'z']

rotate :: Int -> String -> String
rotate _ [] = []
rotate x (s:tr)
  | not . isAlpha $ s = s : rotate x tr
  | otherwise = if charIndex /= Nothing
                then
                  if isUpper s
                  then (toUpper . toLower $ alphabet ! jumpIndex) : rotate x tr
                  else (alphabet ! jumpIndex) : rotate x tr
                else ""
  where
    jumpIndex = fromJust charIndex + x
    charIndex = elemIndex (toLower s) alphabet
