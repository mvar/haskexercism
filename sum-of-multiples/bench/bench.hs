module Main where

import SumOfMultiples
import Criterion
import Criterion.Main

main :: IO ()
main = defaultMain
  [ bgroup "cc" [
      bench "sumOfMultiples"       $ nf sumOfMultiples [3,5]]
--      , bench "som" $ nf som 0 ]
  ]
