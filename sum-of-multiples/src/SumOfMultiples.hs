module SumOfMultiples (sumOfMultiples) where

checkMod :: Integer -> Integer -> Bool
checkMod _ 0 = False
checkMod x y = x `rem` y == 0

sumOfMultiples :: [Integer] -> Integer -> Integer 
sumOfMultiples factors limit = sum [x | x <-[1..limit-1], any (checkMod x) factors]
