module Grains (square, total
              , total0, total1, total2, total3
              , total1List, total2List, total3List)
where

import Control.Applicative (liftA2)
import Data.List (foldl)

square :: Integer -> Maybe Integer
square n
  | n < 1     = Nothing
  | n > 64    = Nothing
  | otherwise = Just (2^(n-1))

extract :: Maybe [a] -> [a]
extract Nothing  = []
extract (Just x) = x

total :: Integer
total = sum (extract (mapM square [1..64]))

-- benchmarking

extract2 :: Maybe Integer -> Integer
extract2 Nothing = 0
extract2 (Just x) = x

total0 :: Integer -> Integer
total0 n = sum (extract (mapM square [1..64]))

total1 :: Integer -> Integer
total1 n = sum (extract (mapM square [n..64]))

total2 :: Integer -> Integer
total2 n = extract2 (foldl (liftA2 (+)) (Just 0) (map square [n..64]))

total3 :: Integer -> Integer
total3 n = foldl (\x y -> x + (extract2 (square y))) 0 [n..64]

total1List :: [Integer] -> Integer
total1List n = sum (extract (mapM square n))

total2List :: [Integer] -> Integer
total2List n = extract2 (foldl (liftA2 (+)) (Just 0) (map square n))

total3List :: [Integer] -> Integer
total3List n = foldl (\x y -> x + (extract2 (square y))) 0 n
