module Main where

import Grains
import Criterion
import Criterion.Main

main :: IO ()
main = defaultMain
  [
    bgroup "weird nf"
    [ bench "total0" $ nf total0 0
    , bench "total1" $ nf total1 1 ]
  , bgroup "weird whnf"
    [ bench "total0" $ nf total0 0
    , bench "total1" $ nf total1 1 ]

  , bgroup "64 elements, nf, integer"
    [ bench "total1" $ nf total1 1
    , bench "total2" $ nf total2 1
    , bench "total3" $ nf total3 1 ]

  , bgroup "64 elements, nf, list"
    [ bench "total1List" $ nf total1List [1..64]
    , bench "total2List" $ nf total2List [1..64]
    , bench "total3List" $ nf total3List [1..64] ]

  , bgroup "128 elements, nf, list"
    [ bench "total1List" $ nf total1List [1..128]
    , bench "total2List" $ nf total2List [1..128]
    , bench "total3List" $ nf total3List [1..128] ]

  , bgroup "64 elements, whnf, integer"
    [ bench "total1" $ whnf total1 1
    , bench "total2" $ whnf total2 1
    , bench "total3" $ whnf total3 1 ]

  , bgroup "64 elements, whnf, list"
    [ bench "total1List" $ whnf total1List [1..64]
    , bench "total2List" $ whnf total2List [1..64]
    , bench "total3List" $ whnf total3List [1..64] ]

  , bgroup "128 elements, whnf, list"
    [ bench "total1List" $ whnf total1List [1..128]
    , bench "total2List" $ whnf total2List [1..128]
    , bench "total3List" $ whnf total3List [1..128] ]
  ]
