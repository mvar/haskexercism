module PrimeFactors (primeFactors) where

primeFactors :: Integer -> [Integer]
primeFactors n = getPrimes n 2

getPrimes :: Integer -> Integer -> [Integer]
getPrimes 0 _ = []
getPrimes 1 _ = []
getPrimes x y =
  if x `rem` y == 0
  then y : getPrimes (x `quot` y) y
  else getPrimes x (y+1)
