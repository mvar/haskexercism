module Series (slices) where
import qualified Data.Vector as V
import Data.Char as C

slices :: Int -> String -> [[Int]]
slices n xs
  | n > length xs = []
  | otherwise = getSlice 0 n (V.fromList xs)

getSlice :: Int -> Int -> V.Vector Char -> [[Int]]
getSlice i len vec
  | i + len > V.length(vec) = V.toList (V.empty)
  | otherwise =
    map C.digitToInt (V.toList (V.slice i len vec)) : (getSlice (i+1) len vec)
