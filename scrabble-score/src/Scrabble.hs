module Scrabble (scoreLetter, scoreWord) where

scoreLetter :: Char -> Integer
scoreLetter letter
  | any (letter ==) "AEIOULNRSTaeioulnrst" = 1
  | any (letter ==) "DGdg" = 2
  | any (letter ==) "BCMPbcmp" = 3
  | any (letter ==) "FHVWYfhvwy" = 4
  | any (letter ==) "Kk" = 5
  | any (letter ==) "JXjx" = 8
  | any (letter ==) "QZqz" = 10
  | otherwise = 0

scoreWord :: String -> Integer
scoreWord word = foldl (+) 0 (map scoreLetter word)
