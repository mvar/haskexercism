{-# LANGUAGE TemplateHaskell #-}
module School (School, add, empty, grade, sorted) where
import Control.Lens hiding (element)
import Data.List

data Student = Student { _gradeNum :: Int, _name :: String } deriving (Eq, Show, Ord)
data School = School { _students :: [Student] } deriving (Eq, Show, Ord)

makeLenses ''Student
makeLenses ''School

getStudentName :: Student -> String
getStudentName = view name

getStudentGrade ::Student -> Int
getStudentGrade = view gradeNum

getSchoolStudents :: School -> [Student]
getSchoolStudents = view students

addStudentToSchool :: Student -> School -> School
addStudentToSchool student school = School ( student : getSchoolStudents school )

add :: Int -> String -> School -> School
add gradeNum student school = addStudentToSchool (Student gradeNum student) school

empty :: School
empty = School([])

studentToTuple :: [Student] -> [(Int, String)]
studentToTuple [] = []
studentToTuple (stu:[]) = [(getStudentGrade stu, getStudentName stu)]
studentToTuple (stu:stus) = (getStudentGrade stu, getStudentName stu) : studentToTuple stus

grade :: Int -> School -> [String]
grade gradeNum school = sort
                        (map (\(a,b) -> b)
                         (filter ((== gradeNum).fst)
                          (studentToTuple (getSchoolStudents school))
                         )
                        )

sorted :: School -> [(Int, [String])]
sorted school = getNames grades school
  where grades = nub
                 (map (\(a,b) -> a)
                  (sortBy (\(a,_) (b,_) -> compare a b)
                   (studentToTuple (getSchoolStudents school))
                  )
                 )

getNames :: [Int] -> School -> [(Int,[String])]
getNames [] school = []          
getNames (x:rest) school = (x, names) : (getNames rest school)
  where names = grade x school

