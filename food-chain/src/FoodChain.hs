module FoodChain (song) where

import Data.Vector as V hiding ((++))

second :: V.Vector [Char]
second = V.fromList ["It wriggled and jiggled and tickled inside her.\n"
                    , "How absurd to swallow a bird!\n"
                    , "Imagine that, to swallow a cat!\n"
                    , "What a hog, to swallow a dog!\n"
                    , "Just opened her throat and swallowed a goat!\n"
                    , "I don't know how she swallowed a cow!\n"
                    , "She's dead, of course!\n"]

stuff :: V.Vector [Char]
stuff = V.fromList ["fly", "spider", "bird", "cat", "dog", "goat", "cow", "horse"]

sfirst :: [Char]
sfirst = "I know an old lady who swallowed a "

slast :: [Char]
slast = "I don't know why she swallowed the fly. Perhaps she'll die.\n"

swallows :: [Char] -> [Char] -> [Char]
swallows sw ca =
  if ca == "spider"
  then "She swallowed the " ++ sw ++ " to catch the " ++ ca ++ " that wriggled and jiggled and tickled inside her.\n"
  else "She swallowed the " ++ sw ++ " to catch the " ++ ca ++ ".\n"

swallower :: Int -> [Char]
swallower 0 = ""
swallower 1 = swallows (stuff ! 1) (stuff ! 0)
swallower x = swallows (stuff ! x) (stuff ! (x-1)) ++ swallower (x-1)

singer :: Int -> [Char]
singer 0 = sfirst ++ (stuff ! 0) ++ ".\n" ++ slast ++ "\n" ++ singer 1
singer 7 = sfirst ++ (stuff ! 7) ++ ".\n" ++ "She's dead, of course!\n"
singer x = sfirst ++ (stuff ! x) ++ ".\n" ++ (second ! (x-1)) ++ swallower x ++ slast ++ "\n" ++ singer (x+1)

song :: String
song = singer 0
