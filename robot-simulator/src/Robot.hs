module Robot
    ( Bearing(East,North,South,West)
    , bearing
    , coordinates
    , mkRobot
    , simulate
    , turnLeft
    , turnRight
    ) where

data Bearing = North
             | East
             | South
             | West
             deriving (Eq, Show)

data Robot = Robot { bear :: Bearing,
                     xpos :: Integer,
                     ypos :: Integer }
           deriving (Eq, Show)

bearing :: Robot -> Bearing
bearing (Robot bear _ _) = bear

coordinates :: Robot -> (Integer, Integer)
coordinates (Robot _ xpos ypos) = (xpos,ypos)

getXpos :: Robot -> Integer
getXpos (Robot _ xpos _) = xpos

getYpos :: Robot -> Integer
getYpos (Robot _ _ ypos) = ypos

xMove :: Bearing -> Integer
xMove b
  | b == East = 1
  | b == West = (-1)
  | otherwise = 0

yMove :: Bearing -> Integer
yMove b
  | b == North = 1
  | b == South = (-1)
  | otherwise  = 0

turnLeft :: Bearing -> Bearing
turnLeft direction
  | direction == North = West
  | direction == West  = South
  | direction == South = East
  | direction == East  = North

turnRight :: Bearing -> Bearing
turnRight direction
  | direction == North = East
  | direction == East  = South
  | direction == South = West
  | direction == West  = North

mkRobot :: Bearing -> (Integer, Integer) -> Robot
mkRobot b (x,y) = Robot { bear = b, xpos = x, ypos = y }

simulate :: Robot -> String -> Robot
simulate robot [] = robot
simulate robot (x:instructions)
  | x == 'R' = simulate robot { bear = turnRight (bearing robot) } instructions
  | x == 'L' = simulate robot { bear = turnLeft  (bearing robot) } instructions
  | x == 'A' = simulate robot { xpos = (getXpos robot) + xMove (bearing robot),
                                ypos = (getYpos robot) + yMove (bearing robot) } instructions
  | otherwise = robot
