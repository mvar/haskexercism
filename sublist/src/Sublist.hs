module Sublist (Sublist(..), sublist) where
import qualified Data.Set as Set
import Data.List (isInfixOf)

data Sublist = Equal | Sublist | Superlist | Unequal deriving (Eq, Show, Ord)

sublist :: Eq a => [a] -> [a] -> Sublist
sublist xs ys
  | xs == ys = Equal
  | isInfixOf xs ys = Sublist
  | isInfixOf ys xs = Superlist
  | otherwise = Unequal

sublistWithSet :: Ord a => [a] -> [a] -> Sublist
sublistWithSet xs ys
  | Set.disjoint f l = Unequal
  | Set.isProperSubsetOf f l = Sublist
  | Set.isSubsetOf f l = Equal
  | Set.isSubsetOf l f = Superlist
  | otherwise = Unequal
  where
    f = Set.fromList xs
    l = Set.fromList ys
