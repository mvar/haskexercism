module BankAccount
  ( BankAccount
  , closeAccount
  , getBalance
  , incrementBalance
  , openAccount
  ) where

import Control.Concurrent.MVar
data BankAccount = BankAccount { b :: MVar (Maybe Integer) } deriving (Eq)

closeAccount :: BankAccount -> IO ()
closeAccount (BankAccount bal) = do
  _ <- swapMVar bal Nothing
  return ()

getBalance :: BankAccount -> IO (Maybe Integer)
getBalance (BankAccount bal) = do
  v <- readMVar bal
  return v

incrementBalance :: BankAccount -> Integer -> IO (Maybe Integer)
incrementBalance (BankAccount bal) amount = do
  v <- readMVar bal
  let n = (+) <$> v <*> (Just amount)
  _ <- swapMVar bal n
  return n

openAccount :: IO BankAccount
openAccount = do
  v <- newEmptyMVar
  putMVar v (Just 0)
  return (BankAccount v)
