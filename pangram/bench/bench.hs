module Main where

import Pangram
import Criterion
import Criterion.Main

main :: IO ()
main = defaultMain
  [ bgroup "isPangram" [ bench "scanl"          $ nf isPangram8 pangram2
                       , bench "foldl simple"   $ nf isPangram10 pangram2
                       , bench "foldl prime"    $ nf isPangram9 pangram2
                       , bench "my fold"        $ nf isPangram pangram2
                       , bench "my fold without if" $ nf isPangram11 pangram2
                       ]
  ]
  where
    pangra = "the quick brown fo jumps over the lay dog"
    pangram1 = "the quick brown fox jumps over the lazy dog"
    pangram2 = concat (replicate 9999 pangra) ++ pangram1
