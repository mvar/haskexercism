module Pangram (isPangram,isPangram8,isPangram9,isPangram10,isPangram11) where
import Data.Char
import qualified Data.Set as Set
import Data.Set (Set)
import Data.List as D

alphabet = Set.fromList ['a'..'z']

isPangram :: String -> Bool
isPangram text = null $ D.foldl (\x y -> if Set.null x then Set.empty else (flip Set.delete) x (toLower y)) alphabet text


isPangram8 :: String -> Bool
isPangram8 = any Set.null . scanl deleteLower alphabet
  where deleteLower cs c = Set.delete (toLower c) cs

isPangram9 :: String -> Bool
isPangram9 = Set.null . foldl' deleteLower alphabet
  where deleteLower cs c = Set.delete (toLower c) cs

isPangram10 :: String -> Bool
isPangram10 = Set.null . foldl deleteLower alphabet
  where deleteLower cs c = Set.delete (toLower c) cs

isPangram11 :: String -> Bool
isPangram11 text = null $ D.foldl (\x y -> (flip Set.delete) x (toLower y)) alphabet text
