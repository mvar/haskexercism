module RailFenceCipher (encode, decode) where
import Data.List
import GHC.Exts (groupWith)

encode :: Int -> String -> String
encode rails str = intercalate "" $ transform2 cipher
  where
    cipher = zip (fencer rails len) str
    len = length str

decode :: Int -> String -> String
decode 1 str = str
decode rails str = fmap snd . sortOn fst . zip ptrn $ str
  where
    -- take a random pattern of letters that patches the zig-zag for the given rails
    -- then map the current encoded string into the pattern and sort on first element
    ptrn = encode rails (take len ['a'..])
    len = length str

fencer :: Int -> Int -> [Int]
fencer n len = take len . cycle $ [1 .. n-1] ++ [n, n-1 .. 2]

transform2 :: [(Int, Char)] -> [String]
transform2 = map (foldr (\(_, c) s -> c:s) "") . (groupWith fst)
