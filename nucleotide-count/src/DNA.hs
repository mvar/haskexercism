{-# LANGUAGE TupleSections #-}
module DNA (nucleotideCounts, Nucleotide(..)) where

import Data.List as L
import Data.Map.Strict (Map, unionWith, fromListWith, map)
import Text.Read

data Nucleotide = A | C | G | T deriving (Enum, Eq, Read, Ord, Show)

ctn :: Char -> Either String Nucleotide
ctn n = readEither [n]

tupleMap :: Integer -> [Nucleotide] -> Map Nucleotide Integer
tupleMap x n = fromListWith (+) (L.map (,x) n)

initial :: Map Nucleotide Integer
initial = tupleMap 0 [A,C,G,T]

-- countNucleotides :: [Nucleotide] -> Either String (Map Nucleotide Integer)
-- countNucleotides str = Right (unionWith (+) (tupleMap 1 str) initial)

countNucleotides :: [Nucleotide] -> (Map Nucleotide Integer)
countNucleotides str = (unionWith (+) (tupleMap 1 str) initial)

nucleotideCounts :: String -> Either String (Map Nucleotide Integer)
nucleotideCounts xs = fmap countNucleotides (mapM ctn xs)
-- nucleotideCounts xs = (mapM ctn xs) >>= countNucleotides
