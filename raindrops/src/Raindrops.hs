module Raindrops (convert) where

factor3 :: Int -> String
factor3 x
  | x `rem` 3 == 0 = "Pling"
  | otherwise = ""

factor5 :: Int -> String
factor5 x
  | x `rem` 5 == 0 = "Plang"
  | otherwise = ""

factor7 :: Int -> String
factor7 x
  | x `rem` 7 == 0 = "Plong"
  | otherwise = ""

convert :: Int -> String
convert n =
  if str == ""
  then show n
  else str
  where str = (factor3 n) ++ (factor5 n) ++ (factor7 n)
