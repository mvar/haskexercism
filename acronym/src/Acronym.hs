module Acronym (abbreviate) where
import Data.Char

abbreviate :: String -> String
abbreviate [] = []
abbreviate (x:y:rest)
  | isUpper x && isUpper y = abbreviate (x:rest)
  | isPunctuation x && isAlpha y = toUpper y : abbreviate rest
  | isPunctuation x && (isAlpha y) == False = abbreviate rest
  | isSpace x = toUpper y : abbreviate rest
abbreviate (x:rest)
  | isUpper x = x : abbreviate rest
  | otherwise = abbreviate rest
