{-# LANGUAGE TemplateHaskell #-}
module Zipper
 ( BinTree(BT)
 , fromTree
 , left
 , right
 , setLeft
 , setRight
 , setValue
 , toTree
 , up
 , value
 ) where

import Data.Maybe
import Control.Lens

-- pretty print
-- import Text.Show.Pretty (ppShow)
-- import Language.Haskell.HsColour
-- import Language.Haskell.HsColour.Colourise
-- import System.IO
-- let colorPrint = putStrLn . hscolour TTY defaultColourPrefs False False "" False . ppShow
-- :set -interactive-print colorPrint

data Direction = L | R deriving (Eq, Show)

data BinTree a = BT { _btValue :: a
                    , _btLeft  :: Maybe (BinTree a)
                    , _btRight :: Maybe (BinTree a)
                    } deriving (Eq, Show)

newtype Zipper a = Zipper (BinTree a, [(Direction, BinTree a)]) deriving (Eq, Show)

makeLenses ''BinTree
makeLenses ''Zipper

fromTree :: (Show a) => BinTree a -> Zipper a
fromTree tree = Zipper (tree, [])

toTree :: (Eq a, Show a) => Zipper a -> BinTree a
toTree (Zipper (current, [])) = current
toTree (Zipper (current, (d, p):revious)) = do
  let newp = case d of
        L -> BT v (Just current) r
        R -> BT v l (Just current)
  toTree (Zipper (newp, revious))
  where
    v = view btValue p
    l = view btLeft p
    r = view btRight p

value :: Zipper a -> a
value (Zipper (current, _)) = view btValue current

left :: (Eq a, Show a) => Zipper a -> Maybe (Zipper a)
left (Zipper (current, previous))
  | btleft == Nothing = Nothing
  | otherwise = Just (Zipper (fromJust btleft, (L, current):previous))
  where
    btleft = view btLeft current

right :: (Eq a, Show a) => Zipper a -> Maybe (Zipper a)
right (Zipper (current, previous))
  | btright == Nothing = Nothing
  | otherwise = Just (Zipper (fromJust btright, (R, current):previous))
  where
    btright = view btRight current

up :: (Eq a, Show a) => Zipper a -> Maybe (Zipper a)
up (Zipper (_, [])) = Nothing
up (Zipper (_, (_, p):revious)) = Just (Zipper (p, revious))

setValue :: (Eq a, Show a) => a -> Zipper a -> Zipper a
setValue x (Zipper (current, [])) = Zipper (current & btValue .~ x, [])
setValue x (Zipper (current, (d, p):revious)) =
  if parentLeft == Just current
  then Zipper (current & btValue .~ x, (d, newPl):revious)
  else Zipper (current & btValue .~ x, (d, newPr):revious)
  where
    parentValue = view btValue p
    parentRight = view btRight p
    parentLeft = view btLeft p
    currentLeft = view btLeft current
    currentRight = view btRight current
    newPl = BT parentValue (Just $ BT x currentLeft currentRight) parentRight
    newPr = BT parentValue parentLeft (Just $ BT x currentLeft currentRight)

setLeft :: (Eq a, Show a) => Maybe (BinTree a) -> Zipper a -> Zipper a
setLeft tree (Zipper (current, [])) = Zipper (current & btLeft .~ tree, [])
setLeft tree (Zipper (current, (d, p):revious)) =
  if parentLeft == Just current
  then Zipper (current & btLeft .~ tree, (d, newPl):revious)
  else Zipper (current & btLeft .~ tree, (d, newPr):revious)
  where
    parentValue = view btValue p
    parentRight = view btRight p
    parentLeft = view btLeft p
    currentValue = view btValue current
    currentRight = view btRight current
    newPl = BT parentValue (Just $ BT currentValue tree currentRight) parentRight
    newPr = BT parentValue parentLeft (Just $ BT currentValue tree currentRight)

setRight :: (Eq a, Show a) => Maybe (BinTree a) -> Zipper a -> Zipper a
setRight tree (Zipper (current, [])) = Zipper (current & btRight .~ tree, [])
setRight tree (Zipper (current, (d, p):revious)) =
  if parentLeft == Just current
  then Zipper (current & btRight .~ tree, (d, newPl):revious)
  else Zipper (current & btRight .~ tree, (d, newPr):revious)
  where
    parentValue = view btValue p
    parentRight = view btRight p
    parentLeft = view btLeft p
    currentValue = view btValue current
    currentLeft = view btLeft current
    newPl = BT parentValue (Just $ BT currentValue currentLeft tree) parentRight
    newPr = BT parentValue parentLeft (Just $ BT currentValue currentLeft tree)
