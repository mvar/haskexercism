module ComplexNumbers ( Complex
                      , conjugate
                      , abs
                      , exp
                      , real
                      , imaginary
                      , mul
                      , add
                      , sub
                      , div
                      , complex ) where

import Prelude hiding (div, abs, exp)
import qualified Prelude as P

-- Data definition -------------------------------------------------------------
data Complex a = Complex a a deriving (Eq, Show)

-- A complex number is a number in the form a + b * i where a and b are real and i satisfies i^2 = -1.
-- a is called the real part and b is called the imaginary part of z.
complex :: (a, a) -> Complex a
complex (a, b) = Complex a b

-- unary operators -------------------------------------------------------------
-- The conjugate of the number a + b * i is the number a - b * i.
conjugate :: Num a => Complex a -> Complex a
conjugate (Complex a b) = Complex a (-b)

-- The absolute value of a complex number z = a + b * i is a real number |z| = sqrt(a^2 + b^2).
abs :: Floating a => Complex a -> a
abs (Complex a b) = sqrt (a*a + b*b)

real :: Num a => Complex a -> a
real (Complex a _) = a

imaginary :: Num a => Complex a -> a
imaginary (Complex _ b) = b

-- Raising e to a complex exponent can be expressed as e^(a + i * b) = e^a * e^(i * b),
-- the last term of which is given by Euler's formula e^(i * b) = cos(b) + i * sin(b).
exp :: Floating a => Complex a -> Complex a
exp (Complex a b) = Complex (cos b * P.exp a) (sin b * P.exp a)

-- binary operators ------------------------------------------------------------
-- Multiplication result is by definition
-- (a + i * b) * (c + i * d) = (a * c - b * d) + (b * c + a * d) * i.
mul :: Num a => Complex a -> Complex a -> Complex a
mul (Complex a b) (Complex c d) = Complex (a * c - b * d) (b * c + a * d)

-- The sum/difference of two complex numbers involves adding/subtracting their real and imaginary parts separately:
-- (a + i * b) + (c + i * d) = (a + c) + (b + d) * i
add :: Num a => Complex a -> Complex a -> Complex a
add (Complex a b) (Complex c d) = Complex (a + c) (b + d)

-- (a + i * b) - (c + i * d) = (a - c) + (b - d) * i
sub :: Num a => Complex a -> Complex a -> Complex a
sub (Complex a b) (Complex c d) = Complex (a - c) (b - d)

-- Dividing a complex number a + i * b by another c + i * d gives:
-- (a + i * b) / (c + i * d) = (a * c + b * d)/(c^2 + d^2) + (b * c - a * d)/(c^2 + d^2) * i.
div :: Fractional a => Complex a -> Complex a -> Complex a
div (Complex a b) (Complex c d) = Complex ((a * c + b * d) / (c*c + d*d)) ((b * c - a * d) / (c*c + d*d))
