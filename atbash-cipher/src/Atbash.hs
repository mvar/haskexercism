module Atbash (decode, encode) where
import qualified Data.Map as M (lookup, fromList)
import Data.List.Split as S (chunksOf)
import Data.Char (isAlphaNum) 
import Data.Maybe (fromJust)
import Data.List (intercalate)

decode :: String -> String
decode cipherText = fromJust . mapM (flip M.lookup decipherMap) $ cipherTextProper
  where
    cipherTextProper = filter (isAlphaNum) cipherText -- inefficient, should use Set, see Pangram solution
    decipherMap = M.fromList (zip cipher plain)
    cipher = "zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA1234567890"
    plain = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz1234567890"

encode :: String -> String
encode plainText = intercalate " " (S.chunksOf 5 (fromJust . mapM (flip M.lookup cipherMap) $ plainTextProper))
  where
    plainTextProper = filter (isAlphaNum) plainText -- same as above
    cipherMap = M.fromList (zip plain cipher)
    cipher = "zyxwvutsrqponmlkjihgfedcbazyxwvutsrqponmlkjihgfedcba1234567890"
    plain = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
