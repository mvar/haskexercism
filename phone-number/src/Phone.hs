module Phone (number) where

import Data.Char (isDigit)
import Data.Maybe (fromJust)

checkLength :: String -> Maybe String
checkLength xs
  | length xs == 10 = Just xs
  | length xs == 11 = Just xs
  | otherwise = Nothing

checkCountry :: String -> Maybe String
checkCountry xs
  | length xs == 11 =
      if head xs == '1' then Just (tail xs) else Nothing
  | otherwise = Just xs

checkCode :: String -> Maybe String
checkCode xs
  | head xs == '0' = Nothing
  | head xs == '1' = Nothing
  | xs !! 3 == '0' = Nothing
  | xs !! 3 == '1' = Nothing
  | otherwise = Just xs

cleanNumber :: String -> String
cleanNumber [] = []
cleanNumber (x:xs)
  | isDigit x = x : cleanNumber xs
  | otherwise = cleanNumber xs

number :: String -> Maybe String
number xs = Just (cleanNumber xs) >>= checkLength >>= checkCountry >>= checkCode
