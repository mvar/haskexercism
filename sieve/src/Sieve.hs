module Sieve (primesUpTo) where
-- You should not use any of the division operations when implementing
-- the sieve of Eratosthenes.
import Prelude hiding (filter, div, mod, divMod, rem, quotRem, quot, (/))
import Control.Monad.ST
import Data.STRef
import Control.Monad.Loops
import Control.Monad
import Data.Vector (Vector, filter, fromList, toList, thaw, freeze)
import Data.Vector.Mutable as VM
import Data.List ((++))

primesUpTo :: Int -> [Int]
primesUpTo n = properPrimes
  where
    properPrimes = toList (filter (/=0) primes)
    primes = getPrimes initList n
    initList = startingList n

startingList :: Int -> Vector Int
startingList n = fromList $ [0,0] ++ [2..n]

-- getPrimes is the equivalent of the following c function:
-- int primes[number+1];
-- for(i = 2; i<=number; i++)
--     primes[i] = i;
-- i = 2;
-- while ((i*i) <= number)
--     if (primes[i] != 0) {
--         for(j=2; j<number; j++) {
--             if (primes[i]*j > number)
--                 break;
--     	else
--                 primes[primes[i]*j]=0;
--         }
--     }
--     i++;

getPrimes :: Vector Int -> Int -> Vector Int
getPrimes v n = runST $ do
  mv <- thaw v
  i  <- newSTRef 2
  whileM_ (do i' <- readSTRef i
              return $ (i'*i') <= n)
          (do i' <- readSTRef i
              p  <- VM.read mv i'
              when (p /= 0) $ (Control.Monad.forM_ [2..n-1] $ \x -> do
                                  if p*x > n
                                    then return()
                                    else VM.write mv (p*x) 0)
              modifySTRef i (+1))
  freeze mv
