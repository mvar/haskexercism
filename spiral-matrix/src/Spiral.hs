module Spiral (spiral) where
import Data.List

getSpiral :: Integer -> Integer -> Integer -> [[Integer]]
getSpiral 0 _ _ = [[]]
getSpiral row col startPos = [startPos .. rowEnd] : (map reverse . transpose $ (getSpiral col nextCol newStartPos))
  where
    rowEnd = startPos + col - 1
    newStartPos = startPos + col
    nextCol = row - 1

spiral :: Integer -> [[Integer]]
spiral 0 = []
spiral 1 = [[1]]
spiral x = getSpiral x x 1
