module House (rhyme) where
import qualified Data.Vector as V

nouns :: V.Vector [Char]
nouns = V.fromList ["the house that Jack built.\n","the malt\n","the rat\n",
                    "the cat\n","the dog\n","the cow with the crumpled horn\n",
                    "the maiden all forlorn\n","the man all tattered and torn\n",
                    "the priest all shaven and shorn\n",
                    "the rooster that crowed in the morn\n",
                    "the farmer sowing his corn\n",
                    "the horse and the hound and the horn\n"]

verbs :: V.Vector [Char]
verbs = V.fromList ["that lay in ","that ate ","that killed ","that worried ",
                    "that tossed ","that milked ","that kissed ","that married ",
                    "that woke ","that kept ","that belonged to ","This is "]

thisIs :: Int -> [Char]
thisIs x = (verbs V.! 11) ++ (nouns V.! x)

thatDid :: Int -> V.Vector [Char]
thatDid x = V.zipWith (++) (V.slice 0 x verbs) (V.slice 0 x nouns)

rhymer :: Int -> [Char]
rhymer x = V.foldl (\y z-> z ++ y) "" vec
  where
    vec = V.snoc (thatDid x) (thisIs x)

fullRhymeGenerator :: Int -> [Char]
fullRhymeGenerator x =
  if x == 11
  then rhymer x
  else rhymer x ++ "\n" ++ (fullRhymeGenerator (x+1))

rhyme :: String
rhyme = fullRhymeGenerator 0

-- oldRhyme :: String
-- oldRhyme = "This is the house that Jack built.\n\
--            \\n\
--            \This is the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the maiden all forlorn\n\
--            \that milked the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the man all tattered and torn\n\
--            \that kissed the maiden all forlorn\n\
--            \that milked the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the priest all shaven and shorn\n\
--            \that married the man all tattered and torn\n\
--            \that kissed the maiden all forlorn\n\
--            \that milked the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the rooster that crowed in the morn\n\
--            \that woke the priest all shaven and shorn\n\
--            \that married the man all tattered and torn\n\
--            \that kissed the maiden all forlorn\n\
--            \that milked the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the farmer sowing his corn\n\
--            \that kept the rooster that crowed in the morn\n\
--            \that woke the priest all shaven and shorn\n\
--            \that married the man all tattered and torn\n\
--            \that kissed the maiden all forlorn\n\
--            \that milked the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n\
--            \\n\
--            \This is the horse and the hound and the horn\n\
--            \that belonged to the farmer sowing his corn\n\
--            \that kept the rooster that crowed in the morn\n\
--            \that woke the priest all shaven and shorn\n\
--            \that married the man all tattered and torn\n\
--            \that kissed the maiden all forlorn\n\
--            \that milked the cow with the crumpled horn\n\
--            \that tossed the dog\n\
--            \that worried the cat\n\
--            \that killed the rat\n\
--            \that ate the malt\n\
--            \that lay in the house that Jack built.\n"
