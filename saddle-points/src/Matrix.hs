module Matrix (saddlePoints) where
import Data.Array
import Data.Maybe

saddlePoints :: Array (Int, Int) Int -> [(Int, Int)]
saddlePoints matrix = [(a,b) | a <- [0..x], b <- [0..y]
                        , matrix ! (a,b) == getRowMaximum matrix a
                        , matrix ! (a,b) == getColMinimum matrix b]
  where
    (_, max) = bounds matrix
    (x, y) = max

getColMinimum :: Array (Int, Int) Int -> Int -> Int
getColMinimum m c = minimum $ map fromJust $ filter (/= Nothing) column
  where
    column = map (\((x,y), z) -> if y == c then Just z else Nothing) (assocs m)

getRowMaximum :: Array (Int, Int) Int -> Int -> Int
getRowMaximum m r = maximum $ map fromJust $ filter (/= Nothing) row
  where
    row = map (\((x,y), z) -> if x == r then Just z else Nothing) (assocs m)
 
