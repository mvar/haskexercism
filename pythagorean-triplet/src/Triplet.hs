module Triplet (tripletsWithSum) where
import Control.Monad (guard)

isPythagorean :: (Int, Int, Int) -> Int -> Bool
isPythagorean (a, b, c) s = squares && sums
  where
    squares = (a*a) + (b*b) == (c*c)
    sums = a + b + c == s

tripletsWithSum :: Int -> [(Int, Int, Int)]
tripletsWithSum s = do
  a <- [3..s]
  b <- [a..s]
  let c = s - a - b
  guard $ isPythagorean (a,b,c) s
  return (a,b,c)
