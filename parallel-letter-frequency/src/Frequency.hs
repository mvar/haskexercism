{-# LANGUAGE TupleSections #-}
module Frequency (frequency, frequencyNoChunks) where

import Data.List.Split (chunksOf)
import Data.List as L (map, foldl, foldr, filter)
import Data.Map.Strict as M (Map, unionWith, fromList, fromListWith, filter, empty)
import Data.Text (Text, unpack)
import Data.Char (toLower, isLetter)
--import Control.Parallel.Strategies

tupleMap :: Int -> [Char] -> Map Char Int
tupleMap x n = M.fromListWith (+) (L.map (,x) n)

initial :: Map Char Int
initial = tupleMap 0 ['a'..'z']

frequencyNoChunks :: Int -> [Text] -> Map Char Int
frequencyNoChunks _ [] = M.fromList []
frequencyNoChunks _ texts = L.foldl ((M.unionWith (+)) . (M.filter (> 0))) initial strTuple
  where
    strTuple = L.map (tupleMap 1) unpackedStrings
    unpackedStrings = L.map ((L.map Data.Char.toLower) . (L.filter isLetter) . unpack) texts

frequency :: Int -> [Text] -> Map Char Int
frequency _ [] = M.fromList []
frequency nWorkers texts = L.foldr (unionWith (+)) empty unite
  where
    unite = L.map (L.foldl ((unionWith (+)) . (M.filter (> 0))) initial) strTuple
    strTuple = L.map (L.map (tupleMap 1)) unpackedStrings
    unpackedStrings = chunksOf nWorkers $ L.map ((L.map toLower) . (L.filter isLetter) . unpack) texts
