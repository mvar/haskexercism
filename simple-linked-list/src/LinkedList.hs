module LinkedList
    ( LinkedList
    , datum
    , fromList
    , isNil
    , new
    , next
    , nil
    , reverseLinkedList
    , toList
    ) where

data LinkedList a = Node a (LinkedList a) | Empty deriving (Eq, Show)

datum :: LinkedList a -> a
datum Empty = error "Empty ll"
datum (Node a _) = a

fromList :: [a] -> LinkedList a
fromList xs = foldr new Empty $ xs

isNil :: LinkedList a -> Bool
isNil Empty = True
isNil (Node _ _) = False

new :: a -> LinkedList a -> LinkedList a
new a ll = Node a ll

next :: LinkedList a -> LinkedList a
next Empty = nil
next (Node _ fwd) = fwd

nil :: LinkedList a
nil = Empty

reverseLinkedList :: LinkedList a -> LinkedList a
reverseLinkedList Empty = Empty
reverseLinkedList ll = retailor Empty ll
  where
    retailor x Empty = x
    retailor x (Node a fwd) = retailor (Node a x) fwd
-- reverseLinkedList ll = fromList . reverse . toList $ ll

toList :: LinkedList a -> [a]
toList Empty = []
toList (Node a fwd) = [a] ++ toList fwd
