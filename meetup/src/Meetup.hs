module Meetup (Weekday(..), Schedule(..), meetupDay) where

import Data.Time.Calendar (Day, fromGregorian, gregorianMonthLength)
import Data.Time.Calendar.WeekDate (toWeekDate)

data Weekday = Monday
             | Tuesday
             | Wednesday
             | Thursday
             | Friday
             | Saturday
             | Sunday
             deriving (Show, Enum, Bounded)

data Schedule = First
              | Second
              | Third
              | Fourth
              | Last
              | Teenth
              deriving (Show, Enum)

meetupDay :: Schedule -> Weekday -> Integer -> Int -> Day
meetupDay schedule weekDay year month = fromGregorian year month day
  where
    day = translateDay schedule weekDay year month

translateDay :: Schedule -> Weekday -> Integer -> Int -> Int
translateDay schedule weekday year month = fds + offset
  where
    (_, _, startingday) = toWeekDate (fromGregorian year month fds)
    fds = case schedule of
             Last -> gregorianMonthLength year month - 6
             Teenth -> 13
             _ -> (fromEnum schedule * 7) + 1
    offset = mod (fromEnum weekday + 1 - startingday) 7
