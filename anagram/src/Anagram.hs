module Anagram (anagramsFor) where
import Data.Char
import Data.List

lowerCaseString :: String -> String
lowerCaseString [] = []
lowerCaseString (x:rest) | isUpper x == True = toLower x : lowerCaseString rest
                         | otherwise = x : lowerCaseString rest

anagramsFor :: String -> [String] -> [String]
anagramsFor xs [] = []
anagramsFor xs (ys:yss)
  | lowerCaseString xs == lowerCaseString ys = anagramsFor xs yss
  | sort (lowerCaseString xs) == sort (lowerCaseString ys) = ys : anagramsFor xs yss
  | otherwise = anagramsFor xs yss
