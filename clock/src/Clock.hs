module Clock (addDelta, fromHourMin, toString) where
import Text.Printf

data Clock = Clock { hours :: Int
                   , minutes :: Int
                   } deriving (Eq,Show)

normalizeHour :: Int -> Int
normalizeHour h
  | h >= 24 = normalizeHour (h - 24)
  | h < 0 = normalizeHour (h + 24)
  | otherwise = h

normalizeTime :: (Int, Int) -> (Int, Int)
normalizeTime (h, m)
  | m >= 60 = normalizeTime ((h + 1), (m - 60))
  | m < 0 = normalizeTime ((h - 1), (m + 60))
  | otherwise = ((normalizeHour h), m)

fromHourMin :: Int -> Int -> Clock
fromHourMin h m = Clock a b
  where
    (a, b) = normalizeTime (h, m)

addDelta :: Int -> Int -> Clock -> Clock
addDelta h m clock = Clock a b
  where
    (a, b) = normalizeTime (hr + h, mn + m)
    hr = hours clock
    mn = minutes clock

toString :: Clock -> String
toString clock = printf "%02d" (hours clock)
                 ++ ":"
                 ++ printf "%02d" (minutes clock)
