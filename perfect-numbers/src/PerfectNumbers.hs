module PerfectNumbers (classify, Classification(..)) where

data Classification = Deficient | Perfect | Abundant deriving (Eq, Show)

intlist = [1..]

isFactor :: Int -> Int -> Bool
isFactor x y
  | x == y = False
  | mod x y == 0 = True
  | otherwise = False

getFactors :: Int -> [Int] -> [Int]
getFactors x [] = []
getFactors x (y:rest)
  | x == y = []
  | isFactor x y == True = y : getFactors x rest
  | otherwise = getFactors x rest

addFactors :: [Int] -> Int
addFactors [] = 0
addFactors (x:rest) = x + addFactors rest

classify :: Int -> Maybe Classification
classify z
  | z <= 0 = Nothing
  | addFactors (getFactors z intlist) == z = Just Perfect
  | addFactors (getFactors z intlist) < z = Just Deficient
  | addFactors (getFactors z intlist) > z = Just Abundant
