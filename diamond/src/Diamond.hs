module Diamond (diamond) where
import Data.Char

alphabet = ['A'..'Z']

printSpaces :: Int -> String
printSpaces x = replicate x ' '

charToDiamondString :: [Char] -> Int -> Int -> [String]
charToDiamondString [] inner side = []
charToDiamondString (x:xs) inner side =
  if x == 'A'
  then (printSpaces side ++ (x:[]) ++ printSpaces side) : charToDiamondString xs (inner + 1) (side - 1)
  else (printSpaces side ++ (x:[]) ++ printSpaces inner ++ (x:[]) ++ printSpaces side) : charToDiamondString xs (inner + 2) (side - 1)

diamond :: Char -> Maybe [String]
diamond char = Just ((charToDiamondString charLst 0 sideSpaces)
                     ++ (tail . reverse $ charToDiamondString charLst 0 sideSpaces))
  where
    charLst = fst (span (<= char) alphabet)
    sideSpaces = (length charLst) - 1
