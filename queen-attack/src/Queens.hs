module Queens (boardString, canAttack) where

import Data.List (intercalate)
import Data.List.Split (chunksOf)
import Data.Array as A (Array, listArray, elems, (//), (!))
import Data.Vector as V (Vector, fromList, toList, (//))
import Data.Maybe (fromJust)

emptyBoard :: Array Int (Vector Char)
emptyBoard = listArray (0,7) $ take 8 $ repeat (fromList "________")

boardToString :: Array Int (Vector Char) -> [String]
boardToString arr = map (addSpaces . V.toList) $ elems arr
  where
    addSpaces s = intercalate " " $ chunksOf 1 s

updateLine :: Array Int (Vector Char) -> (Int, Int) -> Char -> Array Int (Vector Char)
updateLine arr (a,b) c = arr A.// [(a, (arr A.! b V.// [(b, c)]))]

boardString :: Maybe (Int, Int) -> Maybe (Int, Int) -> String
boardString Nothing Nothing = unlines $ boardToString emptyBoard
boardString white black
  | white /= Nothing && black == Nothing = unlines $ boardToString whiteMove
  | white == Nothing && black /= Nothing = unlines $ boardToString blackMove
  | otherwise = unlines $ boardToString bwMove
  where
    whiteMove = updateLine emptyBoard (a, b) 'W'
    blackMove = updateLine emptyBoard (c, d) 'B'
    bwMove = updateLine whiteMove (c, d) 'B'
    (a, b) = fromJust white
    (c, d) = fromJust black

canAttack :: (Int, Int) -> (Int, Int) -> Bool
canAttack (a,b) (c,d)
  | a == c = True
  | b == d = True
  | abs (a - c) == abs (b - d) = True
  | otherwise = False
